<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{

    /**
     * Which columns are fillable for Model ( when using "create" method )
     *
     * @var array
     */
    protected $fillable = ["name"];

    /**
     * Get all books for the author
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function books() {
        return $this->belongsToMany("App\\Book", "author_book", "author_id", "book_id");
    }

    /**
     * If the author doesn't already exist for the passed in $keys ( "name" => "John Doe" for example )
     * create a new Author and return it
     *
     * @param array $data Data to Insert/Update
     * @param array $keys Keys to check for in the table
     * @return Object
     */
    static function createOrRetrieve($data, $keys) {

        $record = self::where($keys)->first();

        if ( ! is_null($record)) return $record;

        return self::create($data);

    }

    /**
     * Set name attribute mutator
     *
     * @param $value
     */
    public function setNameAttribute($value) {
        $this->attributes['name'] = strtolower($value);
    }

    /**
     * Get name attribute
     *
     * @param $value
     * @return string
     */
    public function getNameAttribute($value) {
        return ucwords($value);
    }

}
