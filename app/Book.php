<?php

namespace App;

use App\Traits\FilterableTrait;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use FilterableTrait;

    /**
     * Which columns are fillable for Model ( when using "create" method )
     *
     * @var array
     */
    protected $fillable = [ "title", "language", "original_language", "publishing_year"];

    /**
     * Get the User that created the Book
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo("App\\User");
    }

    /**
     * Get all Authors for the Book
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function authors() {
        return $this->belongsToMany("App\\Author", "author_book", "book_id", "author_id");
    }

}
