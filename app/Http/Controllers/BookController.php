<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{

    /**
     * @var Guard
     */
    protected $auth;

    /**
     * What is the filter "method" for querying DB table if query params are sent for filtering Books
     * Possible values:
     * "exact" - filter by exact value passed in ( only books that match the exact value are returned )
     * "like" - filter using SQL "LIKE" ( all books matching %value% are returned )
     *
     * NOTE: only used when searching for book directly without passing author name to search query
     *       for author + book search, books are always retrieved using "exact" matching
     *
     * @var string
     */
    protected $filter_type = "exact";

    /**
     * Which relationships to eagerly load on our model
     *
     * @var array
     */
    protected $relationships_to_fetch = ["authors"];

    /**
     * @param Guard $auth
     */
    function __construct(Guard $auth)
    {
        $this->auth = $auth;

        $this->middleware("throttle:30");
    }

    /**
     * Return all books if no params for searching are passed in
     * If "title" is passed in as param, filter books by title
     * If "author" is passed in as param, filter to get only that authors books
     * If "author" and "title" are passed as params, get all books by passed in author, then filter to the ones with passed in title
     *
     * All results are paginated, links are sent through JSON as well for "self", "first", "previous", "next" and "last"
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request) {

        $validator = Validator::make($request->all(), [
            "per_page" => "integer|required_with:current_page",
            "current_page" => "integer|required_with:per_page",
            "title" => "string|min:2",
            "author" => "string|min:2"
        ]);

        if($validator->fails()) {
            return response()->json([
                "status" => "failed",
                "errors" => $validator->errors()
            ], 400);
        }

        $per_page = $request->has("per_page") ? $request->per_page : 10;
        $current_page = $request->has("current_page") ? $request->current_page : 1;
        $skip = ($current_page === 1 ) ? 0 : $per_page * ($current_page - 1);

        // If request has ONLY title query string, search for books based on their title
        if($request->has("title") && ! $request->has("author")) {
            return $this->getAllFilteredByTitle($request, $skip, $per_page, $current_page);
        }

        // If request has author get his books, then filter them down based on passed title param if present
        if($request->has("author")) {

            $author = Author::whereName(strtolower($request->author))->first();

            if( ! $author)
                return response()->json([
                    "errors" => false,
                    "status" => "success",
                    "results" => []
                ], 200);

            return $this->getAllFilteredForAuthor($request, $author, $skip, $per_page, $current_page);

        }

        return $this->getAllBooks($request, $skip, $per_page, $current_page);

    }

    /**
     * Create new book resource
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request) {

        // validate params
        $validator = Validator::make($request->all(), [
            "title" => "required|min:2|max:150|unique:books,title,null,id,user_id,{$request->user()->id}",
            "language" => "required|alpha|min:2|max:50",
            "original_language" => "required|alpha|min:2|max:50",
            "publishing_year" => "required|digits:4",
            "authors" => "required|min:2|comma_separated_string"
        ]);

        if($validator->fails()) {
            return response()->json([
                "status" => "failed",
                "errors" => $validator->errors()
            ], 400);
        }

        $book = $this->createBookForUser($request);
        $this->relateAuthorsWithBook($request->authors, $book);

        /*
         * the origin server
           SHOULD send a 201 (Created) response containing a Location header
           field that provides an identifier for the primary resource created
           (Section 7.1.2) and a representation that describes the status of the
           request while referring to the new resource(s).
        */
        return response()->json([
            "status" => "success",
            "errors" => false
        ], 201, [
            "Location" => $request->path() . "/" . $book->id,
        ]);

    }

    /**
     * Update book resource
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id) {

        if( ! $book = Book::find($id)) {
            return response()->json([
                "status" => "failed",
                "errors" => "No book with specified ID found"
            ], 404);
        }

        // validate params
        $validator = Validator::make($request->all(), [
            "title" => "required|min:2|max:150|unique:books,title,null,id,user_id,{$request->user()->id}",
            "language" => "alpha|min:2|max:50",
            "original_language" => "alpha|min:2|max:50",
            "publishing_year" => "digits:4",
            "authors" => "min:2|comma_separated_string"
        ]);

        if($validator->fails()) {
            return response()->json([
                "status" => "failed",
                "errors" => $validator->errors()
            ], 400);
        }

        if($request->user()->cannot("update", $book)) {
            return response()->json([
                "status" => "failed",
                "errors" => "You are not authorized to perform this operation"
            ], 403);
        }

        /*
         * If the target resource does not have a current representation and the
           PUT successfully creates one, then the origin server MUST inform the
           user agent by sending a 201 (Created) response.  If the target
           resource does have a current representation and that representation
           is successfully modified in accordance with the state of the enclosed
           representation, then the origin server MUST send either a 200 (OK) or
           a 204 (No Content) response to indicate successful completion of the
           request.
        */
        $book->update($request->except("authors"));

        if($request->has("authors")) {
            $this->refreshBookAuthorsList($request->authors, $book);
        }

        return response()->json([
            "status" => "success",
            "errors" => false
        ], 200);

    }

    /**
     * Delete book resource
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id) {

        if( ! $book = Book::find($id)) {
            return response()->json([
                "status" => "failed",
                "errors" => "No book with specified ID found"
            ], 404);
        }

        if($request->user()->cannot("destroy", $book)) {
            return response()->json([
                "status" => "failed",
                "errors" => "You are not authorized to perform this operation"
            ], 403);
        }
        /*
         * If a DELETE method is successfully applied, the origin server SHOULD
           send a 202 (Accepted) status code if the action will likely succeed
           but has not yet been enacted, a 204 (No Content) status code if the
           action has been enacted and no further information is to be supplied,
           or a 200 (OK) status code if the action has been enacted and the
           response message includes a representation describing the status.
        */
        $book->delete();

        return response()->json([
            "status" => "success",
            "errors" => false
        ], 200);

    }

    /**
     * Create a new Book and relate it to the user
     *
     * @param Request $request
     * @return mixed
     */
    protected function createBookForUser(Request $request) {
        return $this->auth->user()->books()->save(Book::create($request->except('authors')));
    }

    /**
     * Create ( if not existing ), or just retrieve authors for every passed author and relate them to book
     *
     * @param String $authors
     * @param $book
     */
    protected function relateAuthorsWithBook($authors, $book)
    {
        $authors_array = [];
        foreach(explode(",", rtrim($authors, ",")) as $author_name) {
            $authors_array[] = Author::createOrRetrieve(["name" => trim($author_name)], ["name" => trim($author_name)]);
        }

        $book->authors()->saveMany($authors_array);
    }

    /**
     * Remove all authors for the passed in $book and then attach new ones to it
     *
     * @param $authors
     * @param Book $book
     */
    protected function refreshBookAuthorsList($authors, Book $book) {
        $book->authors()->detach();
        $this->relateAuthorsWithBook($authors, $book);
    }

    /**
     * Get all books for Author and filter them down to only books that have a matching title if title param is present
     *
     * @param Request $request
     * @param $author
     * @param $skip
     * @param $per_page
     *
     * @param $current_page
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getAllFilteredForAuthor(Request $request, $author, $skip, $per_page, $current_page)
    {
        $books = $author->books()->with($this->relationships_to_fetch)->skip($skip)->take($per_page)->get();
        $title_param = "";
        $total_books = $author->books()->count();

        $total_pages = ($per_page >= $total_books) ? 1 : intval(ceil($total_books / $per_page));
        $previous_page = ($current_page == 1) ? 1 : $current_page - 1;
        $next_page = ($current_page >= $total_pages) ? $total_pages : $current_page + 1;

        // If request also has book title in it, search for that particular book
        // that was written by returned author
        if($request->has("title")) {
            $books = $books->where("title", $request->title);
            $title_param = "&title=" . $request->title;

            $total_books = $books->count();

            $total_pages = ($per_page >= $total_books) ? 1 : intval(ceil($total_books / $per_page));
            $previous_page = ($current_page == 1) ? 1 : $current_page - 1;
            $next_page = ($current_page >= $total_pages) ? $total_pages : $current_page + 1;
        }

        return response()->json([
            "errors" => false,
            "status" => "success",
            "results" => $books,
            "links" => [
                "self" => $request->path() . "?current_page={$current_page}&per_page={$per_page}&author={$request->author}{$title_param}",
                "first" => $request->path() . "?current_page=1&per_page={$per_page}&author={$request->author}{$title_param}",
                "previous" => $request->path() . "?current_page={$previous_page}&per_page={$per_page}&author={$request->author}{$title_param}",
                "next" => $request->path() . "?current_page={$next_page}&per_page={$per_page}&author={$request->author}{$title_param}",
                "last" => $request->path() . "?current_page={$total_pages}&per_page={$per_page}&author={$request->author}{$title_param}"
            ]
        ], 200);
    }

    /**
     * Get all books by passed in title param
     *
     * @param Request $request
     * @param $skip
     * @param $per_page
     * @param $current_page
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getAllFilteredByTitle(Request $request, $skip, $per_page, $current_page)
    {
        $filter_method = "filterBy" . ucfirst($this->filter_type);

        $books = Book::$filter_method("title", $request->title)
                ->with($this->relationships_to_fetch)
                ->skip($skip)
                ->take($per_page)
                ->get();

        $total_books = Book::$filter_method("title", $request->title)->count();

        $total_pages = ($per_page >= $total_books) ? 1 : intval(ceil($total_books / $per_page));
        $previous_page = ($current_page == 1) ? 1 : $current_page - 1;
        $next_page = ($current_page >= $total_pages) ? $total_pages : $current_page + 1;

        return response()->json([
            "errors" => false,
            "status"=> "success",
            "results" => $books,
            "links" => [
                "self" => $request->path() . "?current_page={$current_page}&per_page={$per_page}&title={$request->title}",
                "first" => $request->path() . "?current_page=1&per_page={$per_page}&title={$request->title}",
                "previous" => $request->path() . "?current_page={$previous_page}&per_page={$per_page}&title={$request->title}",
                "next" => $request->path() . "?current_page={$next_page}&per_page={$per_page}&title={$request->title}",
                "last" => $request->path() . "?current_page={$total_pages}&per_page={$per_page}&title={$request->title}"
            ]
        ], 200);
    }

    /**
     * Get all books from the DB, paginated
     *
     * @param Request $request
     * @param $skip
     * @param $per_page
     * @param $current_page
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getAllBooks(Request $request, $skip, $per_page, $current_page)
    {
        $books = Book::with($this->relationships_to_fetch)->skip($skip)->take($per_page)->groupBy("title")->orderBy("id")->get();

        $total_books = Book::count();

        $total_pages = ($per_page >= $total_books) ? 1 : intval(ceil($total_books / $per_page));
        $previous_page = ($current_page == 1) ? 1 : $current_page - 1;
        $next_page = ($current_page >= $total_pages) ? $total_pages : $current_page + 1;

        return response()->json([
            "errors" => false,
            "status" => "success",
            "result" => $books,
            "links" => [
                "self" => $request->path() . "?current_page={$current_page}&per_page={$per_page}",
                "first" => $request->path() . "?current_page=1&per_page={$per_page}",
                "previous" => $request->path() . "?current_page={$previous_page}&per_page={$per_page}",
                "next" => $request->path() . "?current_page={$next_page}&per_page={$per_page}",
                "last" => $request->path() . "?current_page={$total_pages}&per_page={$per_page}"]
        ], 200);

    }

}