<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as AuthFactory;

class BasicAuthenticate
{

    protected $auth;

    function __construct(AuthFactory $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string|null $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if($this->auth->guard($guard)->basic('username')) {
            return response()->json([
                "status" => "failed",
                "errors" => "Invalid Credentials"
            ], 401);
        }

        return $next($request);

    }
}
