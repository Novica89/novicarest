<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(["prefix" => "api/v1"], function() {

    // ANY USER
    Route::resource('user', 'UserController', [
        "only" => ["store"]
    ]);

    Route::resource('book', 'BookController', [
        "only" => ["index"]
    ]);

    //Route::get("book", "BookController@store");

    // ONLY FOR LOGGED IN USERS
    Route::group(["middleware" => "auth.basic.rest"], function() {

        Route::resource('book', 'BookController', [
            "only" => ["store", "update", "destroy"]
        ]);

    });

});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
/*
Route::group(['middleware' => ['web']], function () {
    //
});*/
