<?php

namespace App\Policies;

use App\Book;
use App\User;

class BookPolicy
{
    /**
     * Determine if the given book can be updated by user
     *
     * @param  \App\User  $user
     * @param  \App\Book  $book
     * @return bool
     */
    public function update(User $user, Book $book)
    {
        return $user->id == $book->user_id;
    }

    /**
     * Determine if the given book can be deleted by user
     *
     * @param  \App\User  $user
     * @param  \App\Book  $book
     * @return bool
     */
    public function destroy(User $user, Book $book)
    {
        return $user->id == $book->user_id;
    }
}
