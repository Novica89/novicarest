<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class CustomValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend("comma_separated_string", "App\\Services\\CustomValidatorService@validateCommaSeparatedString");
        Validator::replacer("comma_separated_string", "App\\Services\\CustomValidatorService@replaceCommaSeparatedString");
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
