<?php namespace App\Services;

class CustomValidatorService {

    public function validateCommaSeparatedString($attribute, $value, $parameters, $validator) {

        if($value == "") return false;

        // match
        // some
        // some, other
        // some, other, third, fourth
        return preg_match("/^([a-zA-Z0-9 ]+,)+$/", $value);

    }

    public function replaceCommaSeparatedString($message, $attribute, $rule, $parameters) {
        return "The {$attribute} value passed must be a string in the following format: some value, some other value, third value,";
    }

}