<?php  namespace App\Traits;

trait FilterableTrait {

    /**
     * Filter by $field in table which has specified EXACT $value
     *
     * @param $field
     * @param $value
     * @return mixed
     */
    public static function filterByExact($field, $value)
    {
        return self::where($field, $value);
    }

    /**
     * Filter by $field in table which has specified %LIKE% $value
     *
     * @param $field
     * @param $value
     * @return mixed
     */
    public static function filterByLike($field, $value)
    {
        return self::where($field, 'LIKE', "%" .$value. "%");
    }

}