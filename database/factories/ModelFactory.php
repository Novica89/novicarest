<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->userName,
        'password' => 'password',
        'remember_token' => str_random(10),
    ];
});

$languages = ["English", "Russian", "German", "Polish", "Hungarian"];
$original_languages = ["Serbian", "Spanish", "Italian", "Indian"];

$factory->define(App\Book::class, function (Faker\Generator $faker) use ($languages, $original_languages) {
    return [
        'user_id' => factory("App\\User")->create()->id,
        'title' => ucfirst($faker->sentence(3)),
        'language' => $languages[array_rand($languages)],
        'original_language' => $original_languages[array_rand($original_languages)],
        'publishing_year' => intval($faker->year($max = 'now')),
    ];
});

$factory->define(App\Author::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
    ];
});
