# Installation

1. Add an origin repo with Git: git remote add origin https://Novica89@bitbucket.org/Novica89/novicarest.git
2. Fetch all the files from the repo: git pull origin master
3. Run: composer install
4. Open project in IDE and rename .env.example to .env
5. Create a desired DB in phpmyadmin
6. Update .env file to reflect your database settings
7. Run php artisan migrate --seed
8. You're all set

# Usage

## Users

- Create a new user
	POST request to api/v1/user.
	Params: username*, password*
	
## Books

- Get all books created
	GET api/v1/book ( no params )
	
- Get all books created, paginated
	GET api/v1/book ( INT per_page, INT current_page )
	
- Search all books by title
	GET api/v1/book ( STRING title )
	
- Get all books by author
	GET api/v1/book ( STRING author )
	
- Get all books by author, searched by title as well
	GET api/v1/book ( STRING title, STRING author )
	
NOTE: all of the above can be used with pagination params. If you send (per_page, current_page) params
along with other ones as well.

EXAMPLE: GET api/v1/book (title, author, per_page, current_page) request would return all books with "title" 
for "author" and have "per_page" number of results returned via pagination.

- Create new Book
	POST api/v1/book (STRING title, STRING language, STRING original_language, INT(4) publishing_year, STRING authors)
	NOTE: STRING authors must be a comma separated string of authors for the book, format: "author one, author two,"
	
- Update a Book
	PUT api/v1/book/{book_id} ( same params as for creating the book ).
	NOTE: Only title param is required. Authors param, if sent, will replace current books authors with new ones
	
- Delete a Book
	DELETE api/v1/book/{book_id}
	
# Security

- Routes are protected by throttle middleware which allows only 30 requests per 60 seconds to be executed
- HTTP basic auth is implemented for all book related actions, except for GET api/v1/book ones
- One default user is created for the app after migrations are ran with --seed option.
	username: testGuy
	password: password