<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ManageBooksViaApiTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function anyone_can_get_listing_of_existing_books_returned()
    {
        // given we have 10 books by different users
        $books = factory("App\\Book")->times(10)->create();

        // when someone sends a GET API call to 'api/v1/book'
        $response = $this->call('GET','api/v1/book');

        // then he should get back all 10 books as json
        $this->assertJson($books->toJson(), $response->content());

    }

    /**
     * @test
     */
    public function anyone_can_request_paginated_books_listing()
    {
        // given we have 10 books
        factory("App\\Book")->times(10)->create();

        // when someone wants to retrieve paginated set of books
        $this->call('GET','api/v1/book', [
            "per_page" => 5,
            "current_page" => 1
        ]);

        // then he should get back paginated set back with paginate links
        $this->seeJsonStructure([
            "links" => [
                "self","first","previous","next","last"
            ]
        ]);

    }

    /**
     * @test
     */
    public function anyone_can_search_books_by_title()
    {
        // given we have two different books
        factory("App\\Book")->create(["title" => "stupid book"]);
        factory("App\\Book")->create(["title" => "nice book"]);

        // when someone tries to search books by their title
        $this->call('GET','api/v1/book', [
            "title" => "nice book",
        ]);

        // then he should get back only the book(s) he searched for
        $this->seeJsonContains([
            "title" => "nice book"
        ]);
        $this->seeJsonContains([
            "title" => "stupid book"
        ], true);

    }

    /**
     * @test
     */
    public function anyone_can_search_books_by_author()
    {
        // given we have two different books
        $author1 = factory("App\\Author")->create(["name" => "john doe"]);
        $author2 = factory("App\\Author")->create(["name" => "jane doe"]);

        $author_1_books = factory("App\\Book")->times(3)->create();
        $author_2_books = factory("App\\Book")->times(2)->create();

        $author1->books()->saveMany($author_1_books);
        $author2->books()->saveMany($author_2_books);

        // when someone tries to search books by their title
        $this->call('GET','api/v1/book', [
            "author" => "john doe",
        ]);

        // then he should get back only the book(s) he searched for
        $this->seeJsonContains([
            "name" => "John Doe"
        ]);
        $this->seeJsonContains([
            "name" => "Jane Doe"
        ], true);

    }

    /**
     * @test
     */
    public function anyone_can_search_books_by_author_and_title()
    {
        // given we have two different authors
        $author1 = factory("App\\Author")->create(["name" => "john doe"]);
        $author2 = factory("App\\Author")->create(["name" => "jane doe"]);

        // and some books assigned to them
        $author_1_books = factory("App\\Book")->times(3)->create();
        $book_to_search = factory("App\\Book")->create(["title" => "best book ever"]);
        $author_2_books = factory("App\\Book")->times(2)->create();

        $author1->books()->saveMany($author_1_books);
        $author1->books()->save($book_to_search);
        $author2->books()->saveMany($author_2_books);

        // when someone tries to search books by their title AND author
        $this->call('GET','api/v1/book', [
            "author" => "john doe",
            "title" => "best book ever"
        ]);

        // then he should get back only books for the passed author
        // which have the passed in title
        $this->seeJsonContains([
            "title" => "best book ever",
            "name" => "John Doe"
        ]);
        // then he shouldn't get any other books by searched for author that don't have required title
        // or any books by second author
        $this->seeJsonContains([
            "name" => "Jane Doe",
            "title" => $author_1_books[0]->title,
            "title" => $author_1_books[1]->title,
            "title" => $author_1_books[2]->title
        ], true);

    }

    /**
     * @test
     */
    public function existing_user_can_create_new_book_which_gets_assigned_to_him()
    {

        $this->withoutMiddleware();

        // given we have a registered user
        $user = factory("App\\User")->create();
        $this->be($user);
        // when this user tries to create a new book
        $this->call("POST", "api/v1/book", [
            "title" => "Amazing book of REST APIs",
            "language" => "English",
            "original_language" => "Serbian",
            "publishing_year" => "2004",
            "authors" => "Nick Wright,SomeGuy Great,"
        ]);

        // then a new book should be created for that user
        $this->seeJson(["status" => "success"]);
        $this->seeInDatabase("books", [
            "title" => "Amazing book of REST APIs",
            "user_id" => $user->id
        ]);
        $this->seeHeader("Location");

    }

    /**
     * @test
     */
    public function existing_user_can_update_his_book()
    {

        $this->withoutMiddleware();

        // given we have a registered user and one of his books which has some default authors
        $user = factory("App\\User")->create();
        $book = factory("App\\Book")->create(["user_id" => $user->id]);
        $authors = factory("App\\Author")->times(3)->create();
        $book->authors()->saveMany($authors);

        $this->be($user);
        // when this user tries to update this book with another set of authors
        $this->call("PUT", "api/v1/book/{$book->id}", [
            "title" => "Amazing book of REST APIs updated",
            "language" => "English",
            "original_language" => "Serbian",
            "publishing_year" => "1980",
            "authors" => "Novica Vukobratovic, John Doe,"
        ]);

        // then a new book should be updated with the new info and authors and appropriate json and status returned
        $this->seeJson(["status" => "success"]);
        $this->seeInDatabase("books", [
            "title" => "Amazing book of REST APIs updated",
            "user_id" => $user->id
        ]);
        $this->seeInDatabase("authors", [
            "name" => "novica vukobratovic",
            "name" => "john doe"
        ]);

        $this->assertResponseStatus(200);

    }

    /**
     * @test
     */
    public function existing_user_cant_update_someone_elses_book()
    {

        $this->withoutMiddleware();

        // given we have two registered users
        $user = factory("App\\User")->create();
        $user2 = factory("App\\User")->create();

        // and second user has a book created
        $book = factory("App\\Book")->create(["user_id" => $user2->id]);

        // when the first user tries to update second users book
        $this->be($user);
        $this->call("PUT", "api/v1/book/{$book->id}", [
            "title" => "Amazing book of REST APIs updated"
        ]);

        // then he shouldn't be able to do it
        $this->seeJson([
            "status" => "failed"
        ]);

        $this->assertResponseStatus(403);

    }

    /**
     * @test
     */
    public function existing_user_can_delete_his_book()
    {

        $this->withoutMiddleware();

        // given we have a registered user
        $user = factory("App\\User")->create();

        // and a book created for him
        $book = factory("App\\Book")->create(["user_id" => $user->id]);

        // when the user tries to delete it
        $this->be($user);
        $this->call("DELETE", "api/v1/book/{$book->id}");

        // then he should be able to do it
        $this->seeJsonEquals([
            "status" => "success",
            "errors" => false
        ]);

        $this->assertResponseStatus(200);

    }

    /**
     * @test
     */
    public function existing_user_cant_delete_someone_elses_book()
    {

        $this->withoutMiddleware();

        // given we have a registered user
        $user = factory("App\\User")->create();
        $user2 = factory("App\\User")->create();

        // and a book created for him
        $book = factory("App\\Book")->create(["user_id" => $user2->id]);

        // when the user tries to delete it
        $this->be($user);
        $this->call("DELETE", "api/v1/book/{$book->id}");

        // then he should be able to do it
        $this->seeJson([
            "status" => "failed",
        ]);

        $this->assertResponseStatus(403);

    }

}
