<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserRegistrationTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * @test
     */
    public function anyone_can_register_new_account_through_rest_api()
    {
        $this->call("POST", "api/v1/user", [
            'username' => 'johndoe',
            'password' => 'password123'
        ]);

        $this->seeJson([
            "status" => "success",
            "errors" => false
        ]);
        $this->assertResponseStatus(201);

    }

    /**
     * @test
     */
    public function cant_register_two_same_usernames_through_rest_api()
    {

        // having and existing user with username of johndoe
        factory("App\\User")->create([
            'username' => 'johndoe',
            'password' => 'password123'
        ]);

        $this->seeInDatabase('users', ['username' => 'johndoe']);

        // when I try to register a new user with the same username
        $this->call("POST", "api/v1/user", [
            'username' => 'johndoe',
            'password' => 'password123'
        ]);

        // I should get 400 status code
        $this->seeJson([
            "status" => "failed",
        ]);
        $this->assertResponseStatus(400);

    }
}
